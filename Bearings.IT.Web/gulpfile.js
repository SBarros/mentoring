var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var rename = require('gulp-rename');


gulp.task('imagemin', function () {
    gulp.src('./images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('wwwroot/images'));
});

gulp.task('compress', function () {
    gulp.src(require('./vendor-js.json'))
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('wwwroot/js'))
});

gulp.task('minify-css', function () {
    return gulp.src(require('./vendor-css.json'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('wwwroot/css'));
});


gulp.task('default', ['imagemin', 'compress', 'minify-css']);