using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Bearings.IT.Web.Controllers;

namespace Bearings.VG.WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SchoolRegisterController : BaseController
    {
        public SchoolRegisterController() : base()
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Auth()
        {
            return View();
        }
    }
}
