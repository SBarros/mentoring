using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Bearings.IT.Domain.Exceptions;
using Bearings.IT.Services;
using Microsoft.AspNetCore.Authorization;

namespace Bearings.IT.Web.Controllers
{
    public class WelcomeController : BaseController
    {

        public WelcomeController() : base()
        {
        }

        public IActionResult Index()
        {            
            return View();
        }
    }
}
