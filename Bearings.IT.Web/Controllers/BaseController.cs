using Bearings.IT.Domain.Enum;
using Bearings.IT.Domain.Errors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bearings.IT.Web.Controllers
{
    public class BaseController : Controller
    {        
        public BaseController()
        {
        }

        

        // public string BrandId
        // {
        //     get
        //     {                
        //         return User.GetClaim("brandId");
        //     }
        // }
        
        protected BadRequestObjectResult ApiBadRequest(ErrorCode errorCode, string message, string error) {
            return BadRequest(new ApiError(errorCode, message, error));
        }

        protected BadRequestObjectResult ApiBadRequest(ErrorCode errorCode, string message)
        {
            return BadRequest(new ApiError(errorCode, message));
        }

        protected BadRequestObjectResult ApiBadRequest(ErrorCode errorCode, string message, string[] errors)
        {
            return BadRequest(new ApiError(errorCode, message, errors));
        }

    }
}
