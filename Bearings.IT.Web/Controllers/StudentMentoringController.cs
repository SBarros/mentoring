using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Bearings.IT.Domain.Exceptions;
using Bearings.IT.Services;
using Microsoft.AspNetCore.Authorization;
using Bearings.IT.Domain.ViewModels;
using Bearings.IT.Domain.Models;

namespace Bearings.IT.Web.Controllers
{    
    public class StudentMentoringController : BaseController
    {        
        private StudentMentoringService _studentMentoringService;
        public StudentMentoringController(StudentMentoringService studentMentoringService) : base()
        {     
            this._studentMentoringService= studentMentoringService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("~/api/Mentors")]
        public async Task<IActionResult> GetMentors([FromBody] User user)
        {
            //var response = await _studentMentoringService.GetMentorByCourseId(user.CourseOneId, user.CourseTwoId);
            return Ok();
        }

    }
}