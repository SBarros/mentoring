using Bearings.IT.Domain;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bearings.VG.WebApp.Factory
{
    public class DatabaseContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {   
        public DatabaseContext CreateDbContext(string[] args)
        {
            string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            

            
            var builder = new DbContextOptionsBuilder<DatabaseContext>();
            
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                .AddJsonFile("appsettings.json")
                                .AddJsonFile($"appsettings.{env}.json", optional: true)                                
                                .Build();

            builder.UseSqlServer(configuration["Data:ConnectionString"], o=> o.MigrationsAssembly("Bearings.IT.Web"));
                        
            return new DatabaseContext(builder.Options);
        }


    }
}
