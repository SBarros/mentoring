using Bearings.IT.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Bearings.IT.Domain
{

    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> ctx) : base(ctx)
        {
            Database.SetCommandTimeout(TimeSpan.FromMinutes(30));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<StudentMentoring>()
                .HasOne(v => v.Vestibule)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<StudentMentoring> StudentMentorings { get; set; }
        public DbSet<Course> Courses { get; set; }
        
    }
}
