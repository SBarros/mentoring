using Bearings.IT.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bearings.IT.Domain.Errors
{
    public class ApiError
    {
        public ErrorCode status;
        public string message;
        public string[] errors;

        public ApiError()
        {

        }

        public ApiError(ErrorCode status, string message, string[] errors)
        {
            this.status = status;
            this.message = message;
            this.errors = errors;
        }

        public ApiError(ErrorCode status, string message, string error)
        {
            this.status = status;
            this.message = message;
            this.errors = new string[] { error };
        }
        public ApiError(ErrorCode status, string message)
        {
            this.status = status;
            this.message = message;
            this.errors = new string[0];
        }

    }
}
