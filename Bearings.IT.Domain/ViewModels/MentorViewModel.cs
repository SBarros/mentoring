using Bearings.IT.Domain.Models;

namespace Bearings.IT.Domain.ViewModels
{
    public class MentorViewModel
    {
        public string Name { get; set; }
        public int? Age { get; set; }
        public int? Period { get; set; }
        public Course Course { get; set; }
        public string[] Interests { get; set; }
        public string[] AvailableDays { get; set; }
        public string Description { get; set; }
        public double? Avaliation { get; set; }
        public string Image { get; set; }
        
    }
}