using Bearings.IT.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bearings.IT.Domain.Exceptions
{
    public class BaseException : Exception
    {
        public List<string> Messages { get; set; }
        public ErrorCode Code { get; set; }

        public BaseException(ErrorCode errorCode) : base()
        {
            this.Code = errorCode;
        }
        public BaseException(ErrorCode errorCode, string message) : base(message)
        {
            this.Code = errorCode;
            this.Messages = new List<string>(new string[] { message });
        }

        public BaseException(ErrorCode errorCode, List<string> messages)
        {
            this.Code = errorCode;
            this.Messages = messages;
        }
    }
}
