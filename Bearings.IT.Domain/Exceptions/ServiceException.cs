using Bearings.IT.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bearings.IT.Domain.Exceptions
{
    public class ServiceException : BaseException
    {       
        public ServiceException(ErrorCode errorCode) : base(errorCode)
        {

        }
        public ServiceException(ErrorCode errorCode, string message) : base(errorCode, message)
        {
        }        

        public ServiceException(ErrorCode errorCode, List<string> messages) : base(errorCode, messages)
        {            
        }
    }
}
