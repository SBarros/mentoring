using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Bearings.IT.Domain.Models;

namespace Bearings.IT.Domain.Models
{
    public class StudentMentoring
    {
        public int Id { get; set; }
        public int MentorId { get; set; }
        [ForeignKey("MentorId")]
        public User Mentor { get; set; }
        public int VestibuleId { get; set; }
        [ForeignKey("VestibuleId")]
        public User Vestibule { get; set; }
        public int Hours { get; set; }
        public DateTime Appointment { get; set; }
    }
}