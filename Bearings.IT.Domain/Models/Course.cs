using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Bearings.IT.Domain.Models;

namespace Bearings.IT.Domain.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}