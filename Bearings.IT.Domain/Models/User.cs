using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Bearings.IT.Domain.Models;

namespace Bearings.IT.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? LeadScore { get; set; }
        public int CourseOneId { get; set; }
        [ForeignKey("CourseOneId")]
        public Course CourseOne { get; set; }
        public int? CourseTwoId { get; set; }
        [ForeignKey("CourseTwoId")]
        public Course CorseTwo { get; set; }
        public string AvailableDays { get; set; }
        public string MentoringPeriod { get; set; }
        public Boolean IsMentor { get; set; }
        public int? Age { get; set; }
        public int? Period { get; set; }

        public string Interests { get; set; }
        public double? Avaliation { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }
}