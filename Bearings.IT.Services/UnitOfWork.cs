using Bearings.IT.Domain;
using Bearings.IT.Repositories;

namespace Bearings.IT.Services
{
    public class UnitOfWork
    {
        private DatabaseContext _ctx;        

        private StudentMentoringRepository _studentMentoringRepository;

        private UserRepository _userRepository;

        public UnitOfWork(DatabaseContext ctx)
        {
            _ctx = ctx;
        }        

        public UserRepository UserRepository 
        {
            get 
            {
                if(_userRepository == null)
                    _userRepository = new UserRepository(_ctx);
                return _userRepository;
            }
        }
            
    }
}