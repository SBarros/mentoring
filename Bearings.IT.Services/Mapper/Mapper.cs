using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bearings.IT.Services.Interface
{
    public interface IPropertyMapper
    {
        Task<T> Map<T>(object source, object target);
    }
}
