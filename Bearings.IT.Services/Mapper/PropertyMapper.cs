using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Bearings.IT.Services
{
    public class PropertyMapper
    {

        public T Map<T>(object source, object target)
        {

            foreach (var propInfo in target.GetType().GetProperties())
            {
                try
                {                    
                    var sourceProperty = source
                        .GetType()
                        .GetProperty(propInfo.Name);

                    var targetProperty = target.
                        GetType()
                        .GetProperty(propInfo.Name);

                    if (sourceProperty == null || targetProperty == null)
                        continue;

                    if (sourceProperty.GetValue(source) is IList && propInfo.GetType().GetTypeInfo().IsGenericType)
                        continue;

                    var propValue = sourceProperty
                        .GetValue(source);

                    targetProperty.SetValue(target, propValue);
                }
                catch (Exception)
                {
                    //Supress error List to 
                    //return (T)target;
                }
            }

            return (T)target;
        }
    }
}
