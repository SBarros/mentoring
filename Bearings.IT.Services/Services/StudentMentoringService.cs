using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bearings.IT.Domain.Exceptions;
using System.Collections;
using Bearings.IT.Domain.Enum;
using Bearings.IT.Domain.Models;
using Bearings.IT.Domain.ViewModels;

namespace Bearings.IT.Services
{
    public class StudentMentoringService
    {
        private UnitOfWork _uow;

        public StudentMentoringService(UnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<User> GetUserById(int id)
        {
            var student = await _uow.UserRepository.GetById(id);            

            return student;
        }

        public async Task<List<MentorViewModel>> GetMentorByCourseId(int? courseOneId, int? courseTwoId)
        {
            var response = await _uow.UserRepository.GetMentorByCourseIds(courseOneId, courseTwoId);
            List<MentorViewModel> mentors = new List<MentorViewModel>();
            foreach (var r in response) {
                MentorViewModel m = new MentorViewModel();
                m.Name = r.Name;
                m.Age = r.Age;
                m.Avaliation = r.Avaliation;
                m.Course = r.CourseOne;
                m.Description = r.Description;
                m.Image = r.Image;
                m.Period = r.Period;
                m.Interests = r.Interests.Split(',');
                m.AvailableDays = r.AvailableDays.Split(',');
                mentors.Add(m);
            }

            return mentors;
        }

        public void GetMentorByCourseId(int courseOneId, int courseTwoId)
        {
            return;
        }
    }
}
