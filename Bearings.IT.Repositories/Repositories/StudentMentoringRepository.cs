using Bearings.IT.Domain;
using Bearings.IT.Domain.Models;

namespace Bearings.IT.Repositories 
{
    public class StudentMentoringRepository : BaseRepository<StudentMentoring>
    {
        public StudentMentoringRepository (DatabaseContext ctx) : base(ctx)
        {   
        }
    }
}