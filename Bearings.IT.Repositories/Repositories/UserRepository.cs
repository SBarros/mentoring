using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bearings.IT.Domain;
using Bearings.IT.Domain.Models;
using Bearings.IT.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Bearings.IT.Repositories 
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(DatabaseContext ctx) : base(ctx)
        {   
            
        }

         public async Task<User> GetById(int id)
        {
            var user = await entities
                .FirstOrDefaultAsync(a => a.Id == id);

            return user;
        }

        public async Task<List<User>> GetMentorByCourseIds(int? courseOneId, int? courseTwoId)
        {
            var userList = await entities
                .Where(a => (a.CourseOneId == courseOneId || a.CourseTwoId == courseTwoId) &&  a.IsMentor == true).ToListAsync();

            return userList;
        }
    }
}