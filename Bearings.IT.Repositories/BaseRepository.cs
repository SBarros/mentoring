using Bearings.IT.Domain;
using Bearings.IT.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Bearings.IT.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly DatabaseContext context;
        private DbSet<T> _entities;
        protected DbSet<T> entities
        {
            get
            {
                if (_entities == null)
                    _entities = context.Set<T>();
                return _entities;
            }
        }

        public BaseRepository(DatabaseContext _context)
        {
            context = _context;
        }

        public async Task Add(T instance)
        {
            await entities.AddAsync(instance);
        }

        public void Delete(T instance)
        {
            entities.Remove(instance);
        }

        public void Update(T instance)
        {
            this.context.Entry(instance).State = EntityState.Modified;
        }

        public void Dispose()
        {
            if (context != null)
                context.Dispose();
        }

        public async Task<List<T>> Get()
        {
            return await entities.ToListAsync();
        }

        public async Task<T> GetById(object id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<List<T>> GetWith(string[] includes)
        {
            var entitiesHolder = entities.AsQueryable();

            if (includes != null)
            {
                foreach (string include in includes)
                {
                    entitiesHolder = entitiesHolder.Include(include);
                }
            }
            return await entitiesHolder.ToListAsync();
        }

        public async Task<T> GetByIdWith(object id, string[] includes)
        {
            var entitiesHolder = entities.AsQueryable();

            if (includes != null)
            {
                foreach (string include in includes)
                {
                    entitiesHolder = entitiesHolder.Include(include);
                }
            }
            return await entitiesHolder
                .FirstOrDefaultAsync(e => e.GetType().GetProperty("Id").GetValue(e) == id);
        }

        public async Task<bool> Exists(Expression<Func<T, bool>> lambda)
        {
            return await entities.Where(lambda).AnyAsync();
        }

        public DateTime GetFirstMondayOfYear(int year)
        {
            var dt = new DateTime(year, 1, 1);
            while (dt.DayOfWeek != DayOfWeek.Monday)
            {
                dt = dt.AddDays(1);
            }

            return dt;
        }
    }
}
