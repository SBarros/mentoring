using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Bearings.IT.Repositories.Interface
{
    public interface IBaseRepository<T> : IDisposable where T : class
    {
        Task<List<T>> Get();
        Task<List<T>> GetWith(string[] includes);
        Task<T> GetById(object id);
        Task<T> GetByIdWith(object id, string[] includes);
        Task Add(T instance);
        void Delete(T instance);
        Task<bool> Exists(Expression<Func<T, bool>> lambda);
    }
}
